lightdm-remote-session-x2go (0.0.2.0-2) unstable; urgency=medium

  * debian/rules:
    + Fix creation of broken symlink when packaged. (Closes: #877208).
  * debian/control:
    + Bump Standards-Version: to 4.2.0. No changes needed.
    + Drop B-D on dh-autoreconf.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 Aug 2018 22:05:15 +0200

lightdm-remote-session-x2go (0.0.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + Update Maintainer: eMail address: debian-remote@lists.debian.org.
    + Update Vcs-*: fields. Packaging VCS has been moved to salsa.debian.org.
    + Only pyhoca-cli can be used with this LightDM Remote Logon Session.
      Dropping x2goclient as alternative dependency.
    + Sort B-Ds and Ds by name.
    + White-space cleanup at EOLs.
    + Add B: arctica-greeter (<< 0.99.1.0).
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
    + Use secure URI for copyright format reference.
  * debian/watch:
    + Use secure URL to obtain upstream source tarball.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 11 May 2018 13:09:03 +0200

lightdm-remote-session-x2go (0.0.1.1-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/copyright:
    + White-space clean-up.
  * debian/control:
    + Set Priority: from extra to optional to comply with Debian Policy 4.1.0.
    + Bump Standards-Version: to 4.1.0.
  * debian/{control,compat}:
    + Bump DH version level to 10 (and bump B-D to >= 10).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Oct 2017 11:55:05 +0200

lightdm-remote-session-x2go (0.0.1.1-1) experimental; urgency=low

  * Initial upload to Debian. (Closes: #864866).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 16 Jun 2017 10:59:44 +0200
